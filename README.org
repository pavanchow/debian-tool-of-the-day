*** Debian Tool of the Day

* - l

+ Tool of the Days
+ List of Linux tools and their description. 

* - l

** Tool of the Days
   
* htop

#+BEGIN_SRC 
 apt-get install htop
#+END_SRC
   - htop is a simple but very useful tool to understand whats
     currently happening on your computer. In other words its a task
     manager. You can sort processes by memory or CPU. You can also
     see the child and parent processes and even send a SIGKILL to
     kill the process.

* chmod 

#+BEGIN_SRC 
 chmod 777 /home/swecha/Download/develop.txt
#+END_SRC
   - This tool is used to change/give the permissions like
   read,write,excicution Of a file or a directory.
 
   + NOTE: 777 gives read write and execute for everyone

* hdparm

#+BEGIN_SRC 
hdparm -r0 /dev/sdb
#+END_SRC
  - hdparm provides a command line interface to various kernel
    interfaces supported by the Linux SATA/PATA/SAS libata subsystem
    and the older IDE driver subsystem
  - So we can use hdparm to set/remove the READ ONLY tag of the USB drive.
  - Read more https://en.wikipedia.org/wiki/Hdparm

* MAN
#+BEGIN_SRC
 man chmod
#+END_SRC
  - This is a tool that is very much useful to know about a specific
    command i.e what and how is it used.
* du 
#+BEGIN_SRC 
du -h /dir
#+END_SRC
#+BEGIN_EXAMPLE
du -h /dir : provides results in �gHuman Readable Format�g
du -sh /dir : total disk usage size of an directory
#+END_EXAMPLE
  - "du" du is a standard Unix program used to estimate file space
    usage�\space used under a particular directory or files on a file
    system.

* dig
#+BEGIN_SRC 
dig Hostname
#+END_SRC
  - [[https://www.cyberciti.biz/faq/linux-unix-dig-command-examples-usage-syntax/][Dig]] stands for (Domain Information Groper) is a network
    administration command-line tool for querying Domain Name System
    (DNS) name servers. It is useful for verifying and troubleshooting
    DNS problems and also to perform DNS lookups and displays the
    answers that are returned from the name server that were
    queried. dig is part of the BIND domain name server software
    suite. dig command replaces older tool such as nslookup and the
    host. dig tool is available in major Linux distributions.

* PING
#+BEGIN_SRC 
ping host-name/IP
#+END_SRC
  - Ping or Packet Internet Groper is a network administration utility
    used to check the connectivity status between a source and a
    destination computer/device over an IP network. It also helps you
    assess the time it takes to send and receive a response from the
    network.

* Touch
#+BEGIN_SRC 
touch [option] file_name(s)
#+END_SRC
  - The touch command is the easiest way to create new, empty
    files. It is also used to change the timestamps (i.e., dates and
    times of the most recent access and modification) on existing
    files and directories.
* Tasksel
#+BEGIN_SRC 
apt install tasksel
#+END_SRC
  - an user interface for installing tasks
* Xebian
  -  Xfce Debian based system that is much like Xubuntu.
     + It is a distro
* Iw
#+BEGIN_SRC 
iw dev :  lists out the wireless devices.
iw dev wl01 scan : scans the device wl01 for available networks.
#+END_SRC
  - A tool to configure wireless ([[https://wireless.wiki.kernel.org/en/users/documentation/iw][iw]])
* Mosh
 #+BEGIN_SRC 
 mosh user@example.com
 #+END_SRC
 + if you use any other arguments with SSH (such as -p), then
#+BEGIN_SRC 
mosh --ssh="ssh -p 22000" user@example.com
#+END_SRC
  - Mosh (mobile shell) is a tool used to connect from a client
    computer to a server over the Internet, to run a remote
    terminal. Mosh is similar to SSH, with additional features
    meant to improve usability for mobile users.
* whoami
#+BEGIN_SRC 
whoami : Print the user name associated with the current effective user ID.
#+END_SRC
 - [[https://en.m.wikipedia.org/wiki/Whoami][whoami]] displays the username of the current user when this command
   is invoked.
* Xrandr 
#+BEGIN_SRC 
xrandr 
#+END_SRC
#+BEGIN_SRC 
xrandr --help : Lists out the different options that can be used along
the command
#+END_SRC
  - List supported display sizes and set the display device and resolution.

* History (COMMAND)
#+BEGIN_SRC 
history : Displays all the history of the terminal
#+END_SRC

#+BEGIN_SRC 
history | grep apt : Displays  the previously used apt command.
#+END_SRC
#+BEGIN_SRC 
history | grep apt | wc -l : wc -l counts the number of lines.
#+END_SRC

  - Displays the list of commands used before.
* Fsck
  - The system utility [[https://www.tecmint.com/fsck-repair-file-system-errors-in-linux/][fsck]] is a tool for checking the consistency of
    a file system in Unix and Unix-like operating systems, such as
    Linux, macOS, and FreeBSD. A similar command, CHKDSK exists in
    Microsoft Windows
#+BEGIN_SRC 
# fsck /dev/sdb
#+END_SRC
* Rsync
  - Remote file copy (Synchronize file trees) using its own
    protocol. It may be used over an ssh or rsh connection.
#+BEGIN_SRC 
rsync -avz -e ssh user@1.1.1.1:/home/user/Pictures
#+END_SRC
  + -a stands for archive mode
  + -z makes sure the data is compressed during transfer, 
  + -v for verbose( I like it more loud)
* SuperTuxKart
  - SuperTuxKart is a free 3D kart racing game, with a focus on having
    fun over realism. You can play with up to 4 friends on one PC,
    racing against each other or just trying to beat the computer;
    single-player mode is also available.
#+BEGIN_SRC 
apt-get install games-racing :  Installs all racing games 
#+END_SRC
* Sdiff
  - Compare two files side-by-side, optionally merge them
    interactively, and output the results. [[https://www.computerhope.com/unix/usdiff.htm][MORE]]
#+BEGIN_SRC 
sdiff [OPTION]... FILE1 FILE2
#+END_SRC
* Arecord
  - arecord , record voice from microphone.
#+BEGIN_SRC 
arecord FileName.wav : Records the audio and saves it as FileName.wav
#+END_SRC
#+BEGIN_SRC 
aplay FileName.wav : Plays the file FileName.wav
#+END_SRC

* Midnight Commander
  - A powerful file manager([[https://www.linode.com/docs/tools-reference/tools/how-to-install-midnight-commander/][MC]]), Visual shell for Unix-like systems.
#+BEGIN_SRC 
apt install mc
#+END_SRC
#+BEGIN_SRC 
mc
#+END_SRC
* Traceroute
  - Traceroute attempts to trace the route an IP packet would follow
    to some internet host by launching probe packets with a small ttl
    (time to live) then listening for an ICMP "time exceeded" reply
    from a gateway.
#+BEGIN_SRC 
traceroute <sitename>
#+END_SRC
* Eog
  - A GNOME image viewer
#+BEGIN_SRC 
eog [filename]
#+END_SRC
* Pdftk
  - A tool used to combain multiple pdf files into single pdf file using terminal command.
#+BEGIN_SRC 
pdftk file1.pdf file2.pdf cat output mergedfile.pdf
#+END_SRC
#+BEGIN_SRC 
apt install pdftk
#+END_SRC
* Pdfunite
  - A tool used to combain multiple pdf files into single pdf file using above terminal command.
#+BEGIN_SRC 
pdfunite filename1.pdf filename2.pdf filenamen.pdf output.pdf
#+END_SRC
* GnuCash
  - GnuCash is the flagship accountancy software for small
    businesses. Although it�fs only a single-user tool, it can well
    manage the finances of a small business. GnuCash is a double-entry
    accounting tool, with features like checking, funds, stocks,
    possessions, costs, and much more. It offers regular transactions,
    financial procedures, and importing, among other things.
#+BEGIN_SRC 
apt install gnucash
#+END_SRC
* Nmap 
  - Network exploration tool and security / port scanner
    + [[https://highon.coffee/blog/nmap-cheat-sheet/][Nmap]] ("Network Mapper") is a free and open source (license)
      utility for network discovery and security auditing. Many
      systems and network administrators also find it useful for tasks
      such as network inventory, managing service upgrade schedules,
      and monitoring host or service uptime. Nmap uses raw IP packets
      in novel ways to determine what hosts are available on the
      network, what services (application name and version) those
      hosts are offering, what operating systems (and OS versions)
      they are running, what type of packet filters/firewalls are in
      use, and dozens of other characteristics.
#+BEGIN_SRC 
nmap -iL ip-addresses.txt : Scans a list of IP addresses, you can add options before / after.
#+END_SRC
* Free 
  - Free command shows free, total and swap memory information in bytes.
#+BEGIN_SRC 
Free
#+END_SRC
* SSH-copy-id
  - ssh-copy-id is a script that uses ssh(1) to log into a remote
    machine (presumably using a login password, so password
    authentication should be enabled, unless you've done some clever
    use of multiple identities).  It assembles a list of one or more
    fingerprints (as described below) and tries to log in with each
    key, to see if any of them are already installed (of course, if
    you are not using ssh-agent(1) this may result in you being
    repeatedly prompted for pass-phrases).  It then assembles a list
    of those that failed to log in, and using ssh,
    enables logins with those keys on the remote server.  By default it
    adds the keys by appending them to the remote user's
    ~/.ssh/authorized_keys (creating the file, and directory, if
    necessary).  It is also capable of detecting if the remote system is
    a NetScreen, and using its �eset ssh pka-dsa key ...�f command instead.
#+BEGIN_SRC 
man ssh-copy-id
#+END_SRC
